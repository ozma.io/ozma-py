{
  lib,
  stdenv,
  poetry2nix,
  pyright,
  python,
  pkgs,
}: let
  pkg = poetry2nix.mkPoetryApplication {
    projectDir = ./.;
    python = python;
    nativeBuildInputs = [pkgs.poetry pkgs.ruff pkgs.pyright];
  };
in
  pkg
