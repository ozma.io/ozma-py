# Ozma-bot

Ozma-bot is an LLM (Large Language Model) powered chatbot supported by OpenAI.

## Configuration

- Before running the bot, you need to configure the `config.json` file. You can find an example structure for the configuration file in `config.example.json`.
- Turn on all "Privileged Gateway Intents" in the Discord Developer Portal for the bot to work properly.
- Use "OAuth2 URL Generator" in the Discord Developer Portal to generate an invite link for the bot to join your server. "OAuth2" -> "Bot" -> "Administrator" -> "Copy URL" -> Paste in browser -> Select server -> "Authorize".

## Installation

To install and run the bot, follow these steps:

1. Install Docker and Docker Compose.
2. Create a `config.json` file using `config.example.json` as a template and fill in the actual data.
3. Run the bot using the command `docker-compose up`.
