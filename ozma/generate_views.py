from dataclasses import dataclass
from enum import Enum
from typing import Literal
from pydantic import BaseModel

from .types import EntityName, FunDBEntityRef, FunDBUserViewRef, SchemaName, FunDBUserView, UserViewName, funql_name
from .utils import strip_text


class GeneratedEntity(BaseModel):
    caption: str
    columns: list[str]


def _funql_string_char(char: str) -> str:
    if char == "\\":
        return "\\\\"
    elif char == "\b":
        return "\\b"
    elif char == "\f":
        return "\\f"
    elif char == "\n":
        return "\\n"
    elif char == "\r":
        return "\\r"
    elif char == "\t":
        return "\\t"
    elif char == "'":
        return "\\'"
    elif ord(char) < ord(" "):
        return "\\x{:02X}".format(ord(char))
    else:
        return char


def funql_string(string: str) -> str:
    return "'{}'".format("".join((_funql_string_char(char) for char in string)))


def funql_user_view_ref(entity: FunDBUserViewRef) -> str:
    return f"&{funql_name(entity.schema_name)}.{funql_name(entity.name)}"


@dataclass
class FunQLRaw:
    raw: str


# This is a hack to allow `Literal[FUNQL_UNDEFINED]` to be used in `JsonInnerValue`.
class _FunQLUndefined(Enum):
    UNDEFINED = 0


FUNQL_UNDEFINED = _FunQLUndefined.UNDEFINED


JsonValue = None | str | int | float | bool | FunQLRaw | dict[str, "JsonInnerValue"] | list["JsonInnerValue"]
JsonInnerValue = JsonValue | Literal[FUNQL_UNDEFINED]


def funql_json(json: JsonValue, *, pretty_offset: int | None = None, current_offset: int = 0) -> str:
    match json:
        case None:
            return "null"
        case str():
            return funql_string(json)
        case int():
            return str(json)
        case float():
            return str(json)
        case bool():
            return "true" if json else "false"
        case FunQLRaw():
            return json.raw
        case dict():
            filtered_dict = {key: value for key, value in json.items() if value is not FUNQL_UNDEFINED}
            if len(filtered_dict) == 0:
                return "{}"
            elif pretty_offset is None:
                return (
                    "{"
                    + ", ".join(f"{funql_name(key)}: {funql_json(value)}" for key, value in filtered_dict.items())
                    + "}"
                )
            else:
                return (
                    "{\n"
                    + "".join(
                        f"{' ' * (current_offset + pretty_offset)}{funql_name(key)}: {funql_json(value, pretty_offset=pretty_offset, current_offset=current_offset + pretty_offset)},\n"
                        for key, value in filtered_dict.items()
                    )
                    + " " * current_offset
                    + "}"
                )
        case list():
            filtered_list = [value for value in json if value is not FUNQL_UNDEFINED]
            if len(filtered_list) == 0:
                return "[]"
            elif pretty_offset is None:
                return (
                    "[" + ", ".join(funql_json(value, current_offset=current_offset) for value in filtered_list) + "]"
                )
            else:
                return (
                    "[\n"
                    + "".join(
                        f"{' ' * (current_offset + pretty_offset)}{funql_json(value, pretty_offset=pretty_offset, current_offset=current_offset + pretty_offset)},\n"
                        for value in filtered_list
                    )
                    + " " * current_offset
                    + "]"
                )
        case _:
            raise TypeError(f"Invalid JSON value type {type(json)}")


def generate_form(entity: FunDBEntityRef, *, title: str, columns: list[str]) -> str:
    select_columns: list[str] = [
        "@type = 'form'",
        f"@title = {funql_string(title)}",
    ]
    if len(columns) == 0:
        select_columns.append("id")
    else:
        select_columns.extend((funql_name(column) for column in columns))

    select_columns_str = ",\n    ".join(select_columns)
    view = """
        {{
            $id reference({entity})
        }}:

        SELECT
            {select_columns}
        FROM
            {entity}
        WHERE
            id = $id
        FOR INSERT INTO {entity}
    """
    return strip_text(view).format(
        select_columns=select_columns_str,
        entity=entity,
    )


def generate_table(entity: FunDBEntityRef, *, title: str, columns: list[str], form_view: FunDBUserViewRef) -> str:
    select_columns: list[str] = [
        "@type = 'table'",
        f"@title = {funql_string(title)}",
        f"@create_link = {funql_user_view_ref(form_view)}",
        f"@row_link = {funql_user_view_ref(form_view)}",
        "@lazy_load = { pagination: { per_page: 25 }}",
    ]
    if len(columns) == 0:
        select_columns.append("id @{ visible = false }")
    else:
        select_columns.extend((funql_name(column) for column in columns))

    select_columns_str = ",\n    ".join(select_columns)
    view = """
        SELECT
            {select_columns}
        FROM
            {entity}
        ORDER BY
            id
        FOR INSERT INTO {entity}
    """
    return strip_text(view).format(
        select_columns=select_columns_str,
        entity=entity,
    )


class MenuItem(BaseModel):
    name: str
    ref: FunDBUserViewRef
    icon: str | None = None
    new: bool = False


class MenuSubsection(BaseModel):
    name: str
    items: list[MenuItem]


class MenuSection(BaseModel):
    name: str
    subsections: list[MenuSubsection]


MenuStructure = list[MenuSection]


def generate_menu(menu: MenuStructure, *, title: str | None = None) -> str:
    if title is None:
        title = "Menu"

    menu_content: JsonValue = [
        {
            "name": section.name,
            "content": [
                {
                    "name": subsection.name,
                    "size": 3,
                    "content": [
                        {
                            "name": item.name,
                            "ref": FunQLRaw(funql_user_view_ref(item.ref)),
                            "icon": FUNQL_UNDEFINED if item.icon is None else item.icon,
                            "new": FUNQL_UNDEFINED if not item.new else True,
                        }
                        for item in subsection.items
                    ],
                }
                for subsection in section.subsections
            ],
        }
        for section in menu
    ]
    select_columns: list[str] = [
        "@type = 'menu'",
        f"@title = {funql_string(title)}",
        "@menu_centered = true",
        f"{funql_json(menu_content, pretty_offset=4, current_offset=4)} AS menu",
    ]

    select_columns_str = ",\n    ".join(select_columns)
    view = """
        SELECT
            {select_columns}
    """
    return strip_text(view).format(select_columns=select_columns_str)


class SimpleEntity(BaseModel):
    singular_caption: str
    plural_caption: str
    columns: list[str]


class SimpleSchema(BaseModel):
    caption: str
    entities: dict[EntityName, SimpleEntity]


SimpleSchemas = dict[SchemaName, SimpleSchema]


def generate_user_views(schemas: SimpleSchemas, *, user_view_schema: SchemaName) -> dict[UserViewName, FunDBUserView]:
    views: dict[UserViewName, FunDBUserView] = {}
    for schema_name, schema in schemas.items():
        for entity_name, entity in schema.entities.items():
            entity_ref = FunDBEntityRef(schema=schema_name, name=entity_name)
            form_name = f"{schema_name}_{entity_name}_form"
            views[form_name] = FunDBUserView(
                query=generate_form(
                    entity_ref,
                    title=entity.singular_caption,
                    columns=entity.columns,
                ),
            )

            table_name = f"{schema_name}_{entity_name}_table"
            views[table_name] = FunDBUserView(
                query=generate_table(
                    entity_ref,
                    title=entity.plural_caption,
                    columns=entity.columns,
                    form_view=FunDBUserViewRef(schema=user_view_schema, name=form_name),
                ),
            )

    menu_sections = [
        MenuSection(
            name=schema.caption,
            subsections=[
                MenuSubsection(
                    name=entity.plural_caption,
                    items=[
                        MenuItem(
                            name=f"{entity.plural_caption} Table",
                            ref=FunDBUserViewRef(schema=user_view_schema, name=f"{schema_name}_{entity_name}_table"),
                            icon="table_view",
                        ),
                        MenuItem(
                            name=f"New {entity.singular_caption}",
                            ref=FunDBUserViewRef(schema=user_view_schema, name=f"{schema_name}_{entity_name}_form"),
                            icon="add_circle_outline",
                            new=True,
                        ),
                    ],
                )
                for entity_name, entity in schema.entities.items()
            ],
        )
        for schema_name, schema in schemas.items()
    ]
    menu_sections[-1].subsections.append(
        MenuSubsection(
            name="Maintenance",
            items=[
                MenuItem(
                    name="Settings",
                    ref=FunDBUserViewRef(schema="admin", name="main"),
                    icon="settings",
                )
            ],
        )
    )
    views["main"] = FunDBUserView(query=generate_menu(menu_sections))
    return views
