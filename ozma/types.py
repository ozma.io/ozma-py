from datetime import date, datetime, timedelta
from enum import StrEnum
from typing import Annotated, Any, Literal
import isodate
from pydantic import BaseModel, Field, PlainSerializer, model_validator
from pydantic.alias_generators import to_camel


DomainId = int
RowId = int
ArgumentName = str
InstanceName = str
InstanceName = str
SchemaName = str
EntityName = str
FieldName = str
ColumnName = str
IndexName = str
RoleName = str
UserViewName = str
ModuleName = str
ActionName = str
TriggerName = str
CustomEntityName = str
AttributeName = str


FunQLJson = dict[str, Any]
ScalarFunQLValue = str | int | float | bool | date | datetime | timedelta | FunQLJson | None
FunQLValue = ScalarFunQLValue | list[ScalarFunQLValue]
ScalarRawFunQLValue = str | int | float | bool | FunQLJson | None
RawFunQLValue = ScalarRawFunQLValue | list[ScalarRawFunQLValue]


def _funql_scalar_value_to_raw(v: ScalarFunQLValue | ScalarRawFunQLValue) -> ScalarRawFunQLValue:
    match v:
        case date():
            return v.isoformat()
        case datetime():
            return v.isoformat()
        case timedelta():
            return isodate.duration_isoformat(v)
        case _:
            return v


def funql_value_to_raw(v: FunQLValue | RawFunQLValue) -> RawFunQLValue:
    if isinstance(v, list):
        return [_funql_scalar_value_to_raw(x) for x in v]
    else:
        return _funql_scalar_value_to_raw(v)


def _funql_scalar_value_from_raw(
    t: "FunDBScalarValueType", v: ScalarFunQLValue | ScalarRawFunQLValue
) -> ScalarFunQLValue:
    if v is None:
        return None
    match t:
        case FunDBDateValueType():
            match v:
                case date():
                    return v
                case str():
                    return date.fromisoformat(v)
                case _:
                    raise ValueError(f"Expected date string, got {v}")
        case FunDBDateTimeValueType() | FunDBLocalDateTimeValueType():
            match v:
                case datetime():
                    return v
                case str():
                    return datetime.fromisoformat(v)
                case _:
                    raise ValueError(f"Expected datetime string, got {v}")
        case FunDBIntervalValueType():
            match v:
                case timedelta():
                    return v
                case str():
                    return isodate.parse_duration(v)
                case _:
                    raise ValueError(f"Expected interval string, got {v}")
        case _:
            return v


def funql_value_from_raw(t: "FunDBValueType", v: FunQLValue | RawFunQLValue) -> FunQLValue:
    if isinstance(t, FunDBArrayValueType):
        if not isinstance(v, list):
            raise ValueError(f"Expected list, got {v}")
        return [_funql_scalar_value_from_raw(t.subtype, x) for x in v]
    else:
        if isinstance(v, list):
            raise ValueError(f"Expected scalar value, got {v}")
        return _funql_scalar_value_from_raw(t, v)


FunQLValueArg = Annotated[FunQLValue | RawFunQLValue, PlainSerializer(funql_value_to_raw, return_type=RawFunQLValue)]


class FunDBModel(BaseModel, alias_generator=to_camel, populate_by_name=True, extra="ignore"):
    pass


def funql_name(name: str) -> str:
    return '"{}"'.format(name.replace('"', '""'))


class FunDBAbstractRef(BaseModel, alias_generator=to_camel, populate_by_name=True, frozen=True):
    # `schema` conflicts with `schema` in `BaseModel`.
    schema_name: SchemaName = Field(alias="schema")
    name: str

    # Allow positional args.
    def __init__(
        self, schema_name: SchemaName | None = None, name: str | None = None, schema: SchemaName | None = None
    ):
        if schema_name and schema:
            raise ValueError("Use schema_name or schema")
        super().__init__(schema_name=schema_name or schema, name=name)

    def __str__(self) -> str:
        return f"{funql_name(self.schema_name)}.{funql_name(self.name)}"


class FunDBEntityRef(FunDBAbstractRef, frozen=True):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)


class FunDBUserViewRef(FunDBAbstractRef, frozen=True):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)


class FunDBFieldRef(BaseModel, alias_generator=to_camel, populate_by_name=True, frozen=True):
    entity: FunDBEntityRef
    name: str

    # Allow positional args.
    def __init__(self, entity: FunDBEntityRef, name: str):
        super().__init__(entity=entity, name=name)

    def __str__(self) -> str:
        return f"{self.entity}.{funql_name(self.name)}"


class FunDBColumnField(FunDBModel):
    type: str = Field(description="The type of the column.")
    default_value: str | None = Field(default=None, description="Constant FunQL expression for the default value.")
    is_nullable: bool = False
    is_immutable: bool = False
    description: str = ""
    metadata: dict[str, Any] = Field(default_factory=dict)


class FunDBComputedField(FunDBModel):
    expression: str = Field(description="FunQL expression.")
    allow_broken: bool = False
    is_virtual: bool = False
    is_materialized: bool = False
    description: str = ""
    metadata: dict[str, Any] = Field(default_factory=dict)


class FunDBUniqueConstraint(FunDBModel):
    columns: list[str]
    is_alternate_key: bool = False
    description: str = ""
    metadata: dict[str, Any] = Field(default_factory=dict)


class FunDBCheckConstraint(FunDBModel):
    expression: str


class IndexType(StrEnum):
    BTREE = "btree"
    GIST = "gist"
    GIN = "gin"


class FunDBIndex(FunDBModel):
    expressions: list[str] = Field(description="FunQL expressions list to index.")
    included_expressions: list[str] = Field(default_factory=list)
    is_unique: bool = False
    type: IndexType = IndexType.BTREE
    predicate: str | None = None
    description: str = ""
    metadata: dict[str, Any] = Field(default_factory=dict)


class FunDBEntity(FunDBModel):
    column_fields: dict[FieldName, FunDBColumnField] = Field(default_factory=dict)
    computed_fields: dict[FieldName, FunDBComputedField] = Field(default_factory=dict)
    unique_constraints: dict[FieldName, FunDBUniqueConstraint] = Field(default_factory=dict)
    check_constraints: dict[FieldName, FunDBCheckConstraint] = Field(default_factory=dict)
    indexes: dict[IndexName, FunDBIndex] = Field(default_factory=dict)
    main_field: str | None = None
    save_restore_key: str | None = None
    is_abstract: bool = False
    is_frozen: bool = False
    parent: FunDBEntityRef | None = None
    description: str = ""
    metadata: dict[str, Any] = Field(default_factory=dict)


class FunDBAttributesField(FunDBModel):
    allow_broken: bool = False
    priority: int = 0
    attributes: str


class FunDBAttributesEntity(FunDBModel):
    fields: dict[FieldName, FunDBAttributesField] = Field(default_factory=dict)


class FunDBAttributesSchema(FunDBModel):
    entities: dict[EntityName, FunDBAttributesEntity] = Field(default_factory=dict)


class FunDBSavedSchemaCustomEntities(FunDBModel):
    type: Literal["customEntities"] = "customEntities"
    custom_entities: dict[SchemaName, dict[EntityName, list[Any]]] = Field(default_factory=dict)


class FunDBUserView(FunDBModel):
    query: str
    allow_broken: bool = False


class FunDBFullSavedSchema(FunDBSavedSchemaCustomEntities):
    type: Literal["full"] = "full"
    entities: dict[EntityName, FunDBEntity] = Field(default_factory=dict)
    # TODO: Type others.
    roles: dict[RoleName, Any] = Field(default_factory=dict)
    user_views: dict[UserViewName, FunDBUserView] = Field(default_factory=dict)
    default_attributes: dict[SchemaName, FunDBAttributesSchema] = Field(default_factory=dict)
    modules: dict[ModuleName, Any] = Field(default_factory=dict)
    actions: dict[ActionName, Any] = Field(default_factory=dict)
    triggers: dict[TriggerName, Any] = Field(default_factory=dict)
    user_views_geneerator_script: str | None = None
    description: str = ""
    metadata: dict[str, Any] = Field(default_factory=dict)


FunDBSavedSchema = Annotated[FunDBFullSavedSchema | FunDBSavedSchemaCustomEntities, Field(discriminator="type")]


class FunDBSavedSchemas(FunDBModel):
    schemas: dict[SchemaName, FunDBSavedSchema] = Field(default_factory=dict)


class FunDBActionResponse(FunDBModel):
    result: RawFunQLValue = None


class FunDBInsertEntityOp(FunDBModel):
    type: Literal["insert"] = "insert"
    entity: FunDBEntityRef
    fields: dict[FieldName, FunQLValueArg]


class FunDBUpdateEntityOp(FunDBModel):
    type: Literal["update"] = "update"
    entity: FunDBEntityRef
    id: RowId
    fields: dict[FieldName, FunQLValueArg]


class FunDBDeleteEntityOp(FunDBModel):
    type: Literal["delete"] = "delete"
    entity: FunDBEntityRef
    id: RowId


FunDBTransactionOp = Annotated[
    FunDBInsertEntityOp | FunDBUpdateEntityOp | FunDBDeleteEntityOp, Field(discriminator="type")
]


class FunDBInsertEntityResult(FunDBModel):
    type: Literal["insert"] = "insert"
    id: RowId | None


class FunDBUpdateEntityResult(FunDBModel):
    type: Literal["update"] = "update"
    id: RowId


class FunDBDeleteEntityResult(FunDBModel):
    type: Literal["delete"] = "delete"


FunDBTransactionResultOp = Annotated[
    FunDBInsertEntityResult | FunDBUpdateEntityResult | FunDBDeleteEntityResult, Field(discriminator="type")
]


class FunDBTransactionResult(FunDBModel):
    results: list[FunDBTransactionResultOp]


class FunDBIntValueType(FunDBModel):
    type: Literal["int"] = "int"


class FunDBDecimalValueType(FunDBModel):
    type: Literal["decimal"] = "decimal"


class FunDBStringValueType(FunDBModel):
    type: Literal["string"] = "string"


class FunDBBoolValueType(FunDBModel):
    type: Literal["bool"] = "bool"


class FunDBDateTimeValueType(FunDBModel):
    type: Literal["datetime"] = "datetime"


class FunDBLocalDateTimeValueType(FunDBModel):
    type: Literal["localdatetime"] = "localdatetime"


class FunDBDateValueType(FunDBModel):
    type: Literal["date"] = "date"


class FunDBIntervalValueType(FunDBModel):
    type: Literal["interval"] = "interval"


class FunDBJsonValueType(FunDBModel):
    type: Literal["json"] = "json"


class FunDBUuidValueType(FunDBModel):
    type: Literal["uuid"] = "uuid"


FunDBScalarValueType = Annotated[
    FunDBIntValueType
    | FunDBDecimalValueType
    | FunDBStringValueType
    | FunDBBoolValueType
    | FunDBDateTimeValueType
    | FunDBLocalDateTimeValueType
    | FunDBDateValueType
    | FunDBIntervalValueType
    | FunDBJsonValueType
    | FunDBUuidValueType,
    Field(discriminator="type"),
]


class FunDBArrayValueType(FunDBModel):
    type: Literal["array"] = "array"
    subtype: FunDBScalarValueType


# FunDBValueType conversion
FunDBValueType = Annotated[FunDBScalarValueType | FunDBArrayValueType, Field(discriminator="type")]


class FunDBIntFieldType(FunDBModel):
    type: Literal["int"] = "int"


class FunDBDecimalFieldType(FunDBModel):
    type: Literal["decimal"] = "decimal"


class FunDBStringFieldType(FunDBModel):
    type: Literal["string"] = "string"


class FunDBBoolFieldType(FunDBModel):
    type: Literal["bool"] = "bool"


class FunDBDateTimeFieldType(FunDBModel):
    type: Literal["datetime"] = "datetime"


class FunDBDateFieldType(FunDBModel):
    type: Literal["date"] = "date"


class FunDBIntervalFieldType(FunDBModel):
    type: Literal["interval"] = "interval"


class FunDBJsonFieldType(FunDBModel):
    type: Literal["json"] = "json"


class FunDBUuidFieldType(FunDBModel):
    type: Literal["uuid"] = "uuid"


class FunDBReferenceFieldType(FunDBModel):
    type: Literal["reference"] = "reference"
    entity: FunDBEntityRef


class FunDBEnumFieldType(FunDBModel):
    type: Literal["enum"] = "enum"
    values: list[str]


FunDBScalarFieldType = Annotated[
    FunDBIntFieldType
    | FunDBDecimalFieldType
    | FunDBStringFieldType
    | FunDBBoolFieldType
    | FunDBDateTimeFieldType
    | FunDBDateFieldType
    | FunDBIntervalFieldType
    | FunDBJsonFieldType
    | FunDBUuidFieldType
    | FunDBReferenceFieldType
    | FunDBEnumFieldType,
    Field(discriminator="type"),
]


# ArrayFieldType
class FunDBArrayFieldType(FunDBModel):
    type: Literal["array"] = "array"
    subtype: FunDBScalarFieldType


# Union for FieldType
FunDBFieldType = Annotated[FunDBScalarFieldType | FunDBArrayFieldType, Field(discriminator="type")]


def fundb_scalar_field_type_to_type(type: FunDBScalarFieldType) -> FunDBScalarValueType:
    # Ugly!
    match type:
        case FunDBIntFieldType():
            return FunDBIntValueType()
        case FunDBDecimalFieldType():
            return FunDBDecimalValueType()
        case FunDBStringFieldType():
            return FunDBStringValueType()
        case FunDBBoolFieldType():
            return FunDBBoolValueType()
        case FunDBDateTimeFieldType():
            return FunDBDateTimeValueType()
        case FunDBDateFieldType():
            return FunDBDateValueType()
        case FunDBIntervalFieldType():
            return FunDBIntervalValueType()
        case FunDBJsonFieldType():
            return FunDBJsonValueType()
        case FunDBUuidFieldType():
            return FunDBUuidValueType()
        case FunDBReferenceFieldType():
            return FunDBIntValueType()
        case FunDBEnumFieldType():
            return FunDBStringValueType()
        case _:
            raise ValueError(f"Invalid scalar field type {type}")


def fundb_field_type_to_value(type: FunDBFieldType) -> FunDBValueType:
    if isinstance(type, FunDBArrayFieldType):
        return FunDBArrayValueType(subtype=fundb_scalar_field_type_to_type(type.subtype))
    else:
        return fundb_scalar_field_type_to_type(type)


class FunDBFieldAccess(FunDBModel):
    select: bool
    update: bool
    insert: bool


class FunDBViewColumnField(FunDBModel):
    field_type: FunDBFieldType
    value_type: FunDBValueType
    default_value: FunQLValue | None = None
    is_nullable: bool
    is_immutable: bool
    inherited_from: FunDBEntityRef | None = None
    access: FunDBFieldAccess
    hasUpdateTriggers: bool

    @model_validator(mode="after")
    def validate(self) -> "FunDBViewColumnField":
        if self.default_value is not None:
            self.default_value = funql_value_from_raw(self.value_type, self.default_value)
        return self


class FunDBMainFieldInfo(FunDBModel):
    name: FieldName
    field: FunDBViewColumnField


class FunDBBoundMappingEntry(FunDBModel):
    when: FunQLValue
    value: FunQLValue

    def _convert_values(self, type: FunDBValueType):
        self.when = funql_value_from_raw(type, self.when)
        self.value = funql_value_from_raw(type, self.value)


class FunDBBoundMapping(FunDBModel):
    entries: list[FunDBBoundMappingEntry]
    default: FunQLValue | None = None

    def _convert_values(self, type: FunDBValueType):
        for entry in self.entries:
            entry._convert_values(type)
        if self.default is not None:
            self.default = funql_value_from_raw(type, self.default)


class FunDBBasicAttributeInfo(FunDBModel):
    type: FunDBValueType


class FunDBMappedAttributeInfo(FunDBBasicAttributeInfo):
    mapping: FunDBBoundMapping | None = None

    @model_validator(mode="after")
    def _validate(self) -> "FunDBMappedAttributeInfo":
        if self.mapping is not None:
            self.mapping._convert_values(self.type)
        return self


class FunDBBoundAttributeInfo(FunDBMappedAttributeInfo):
    const: bool = False


class FunDBCellAttributeInfo(FunDBMappedAttributeInfo):
    pass


class FunDBViewAttributeInfo(FunDBBasicAttributeInfo):
    const: bool = False


class FunDBRowAttributeInfo(FunDBBasicAttributeInfo):
    pass


class FunDBResultColumnInfo(FunDBModel):
    name: str
    attribute_types: dict[str, FunDBBoundAttributeInfo] = Field(default_factory=dict)
    cell_attribute_types: dict[str, FunDBCellAttributeInfo] = Field(default_factory=dict)
    value_type: FunDBValueType
    pun_type: FunDBValueType | None = None
    main_field: FunDBMainFieldInfo | None = None


class FunDBDomainField(FunDBModel):
    ref: FunDBFieldRef
    field: FunDBViewColumnField | None = None
    id_column: int


class FunDBArgument(FunDBModel):
    name: str
    arg_type: FunDBFieldType
    optional: bool
    default_value: FunQLValue | None = None
    attribute_types: dict[str, FunDBBoundAttributeInfo] = Field(default_factory=dict)

    @model_validator(mode="after")
    def validate(self) -> "FunDBArgument":
        arg_value_type = fundb_field_type_to_value(self.arg_type)
        if self.default_value is not None:
            self.default_value = funql_value_from_raw(arg_value_type, self.default_value)
        return self


class FunDBMainEntity(FunDBModel):
    entity: FunDBEntityRef
    for_insert: bool


class FunDBResultViewInfo(FunDBModel):
    attribute_types: dict[str, FunDBViewAttributeInfo] = Field(default_factory=dict)
    row_attribute_types: dict[str, FunDBRowAttributeInfo] = Field(default_factory=dict)
    arguments: list[FunDBArgument]
    domains: dict[str, dict[str, FunDBDomainField]] = Field(default_factory=dict)
    main_entity: FunDBMainEntity | None = None
    columns: list[FunDBResultColumnInfo]
    hash: str


AttributesMap = dict[AttributeName, FunQLValue]


class FunDBExecutedValue(FunDBModel):
    value: FunQLValue
    attributes: AttributesMap | None = None
    pun: FunQLValue | None = None

    def _convert_values(self, column: FunDBResultColumnInfo):
        self.value = funql_value_from_raw(column.value_type, self.value)
        if self.pun is not None:
            assert column.pun_type is not None
            self.pun = funql_value_from_raw(column.pun_type, self.pun)
        if self.attributes is not None:
            for name, value in self.attributes.items():
                self.attributes[name] = funql_value_from_raw(column.attribute_types[name].type, value)


class FunDBEntityId(FunDBModel):
    id: RowId
    sub_entity: FunDBEntityRef | None = None


class FunDBExecutedRow(FunDBModel):
    values: list[FunDBExecutedValue]
    domain_id: DomainId | None = None
    attributes: AttributesMap | None = None
    entity_ids: dict[ColumnName, FunDBEntityId] | None = None
    main_id: RowId | None = None
    main_sub_entity: FunDBEntityRef | None = None

    def _convert_values(self, info: FunDBResultViewInfo):
        for column, value in zip(info.columns, self.values):
            value._convert_values(column)
        if self.attributes is not None:
            for name, value in self.attributes.items():
                self.attributes[name] = funql_value_from_raw(info.row_attribute_types[name].type, value)


class FunDBExecutedViewExpr(FunDBModel):
    attributes: AttributesMap
    column_attributes: list[AttributesMap]
    argument_attributes: dict[ArgumentName, AttributesMap]
    rows: list[FunDBExecutedRow]

    def _convert_values(self, info: FunDBResultViewInfo):
        for row in self.rows:
            row._convert_values(info)
        for name, value in self.attributes.items():
            self.attributes[name] = funql_value_from_raw(info.attribute_types[name].type, value)
        for column, attributes in zip(info.columns, self.column_attributes):
            for name, value in attributes.items():
                attributes[name] = funql_value_from_raw(column.attribute_types[name].type, value)
        for argument in info.arguments:
            attributes = self.argument_attributes.get(argument.name)
            if attributes is not None:
                for name, value in attributes.items():
                    attributes[name] = funql_value_from_raw(argument.attribute_types[name].type, value)


class FunDBViewExprResult(FunDBModel):
    info: FunDBResultViewInfo
    result: FunDBExecutedViewExpr

    @model_validator(mode="after")
    def _validate(self) -> "FunDBViewExprResult":
        self.result._convert_values(self.info)
        return self


class FunDBViewInfoResult(FunDBModel):
    info: FunDBResultViewInfo
    const_attributes: AttributesMap
    const_column_attributes: list[AttributesMap]
    const_argument_attributes: dict[ArgumentName, AttributesMap]

    @model_validator(mode="after")
    def _validate(self) -> "FunDBViewInfoResult":
        for name, value in self.const_attributes.items():
            self.const_attributes[name] = funql_value_from_raw(self.info.attribute_types[name].type, value)
        for column, attributes in zip(self.info.columns, self.const_column_attributes):
            for name, value in attributes.items():
                attributes[name] = funql_value_from_raw(column.attribute_types[name].type, value)
        for argument in self.info.arguments:
            attributes = self.const_argument_attributes.get(argument.name)
            if attributes is not None:
                for name, value in attributes.items():
                    attributes[name] = funql_value_from_raw(argument.attribute_types[name].type, value)
        return self


class FunDBChunkArgument(FunDBModel):
    type: str
    value: FunQLValueArg


class FunDBChunkWhere(FunDBModel):
    arguments: dict[ArgumentName, FunDBChunkArgument] | None = None
    expression: str


class FunDBQueryChunk(FunDBModel):
    limit: int | None = None
    offset: int | None = None
    where: FunDBChunkWhere | None = None
    search: str | None = None
